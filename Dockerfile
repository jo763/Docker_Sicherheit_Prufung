FROM nginx:1.18.0-perl

COPY 40-generate-cert.sh /docker-entrypoint.d/

COPY nginx.conf /etc/nginx/
COPY ./html/ /var/www/html/

ENV NGINX_ENTRYPOINT_QUIET_LOGS=1
