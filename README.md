# docker-sec-example

This repository contains a Dockerfile which extends the official nginx container image to create a microservice. The microservice presents a website advertising some example services.

To build the container locally:

`docker build --tag local/sec-example .`

To run the container in the foreground:

`docker run -p 80:80 -p 443:443 --interactive --tty local/sec-example`

Once the container is running you should be able to navigate to http://localhost or https://localhost to view it.

Note about ports:

* If you're attempting to use privileged ports you may find that you get an error when starting the container, if this is the case I suggest you use the following instead:
  * `docker run -p 8080:80 -p 8443:443 --interactive --tty local/sec-example`
  * Navigate to http://localhost:8080 or https://localhost:8443

The purpose of this exercise is to ensure a secure workload is being deployed for this microservice. You should undertake the following tasks:

* Extend the GitLab CI pipeline to run appropriate scans to ensure sufficient security
  * It's up to you what tooling you use; please research, select appropriately and document your reasons
* Write down at least 2 general advisories for improving the security of the container (i.e. not the output of the security tooling)

**Important notes:**

* Please don't spend longer than 2 hours completing this task.
* Don't hide your test commits or pipelines - it's very useful to see the evolution of your creation.
* You don't need to fix the issues that are uncovered.

*Hint: Check out what tooling is provided by GitLab first*


# Answers

**Appropriate scans inclusion and justification**


| Types of CI Tests                               | Description | Justification |
| -----------                                     | ----------- | ------------ |
| Static Application Security Testing (SAST)      | Checks source code for known vulnerabilities.  | Included as practical test |
| Dynamic Application Security Testing (DAST)     | Examines applications for vulnerabilities like those in deployed environments  | Not included as don't have ultimate and requires test environment but would include| 
| Infrastructure as Code (IaC) Scanning           | Scans IaC configuration files for known vulnerabilities. | Not included as no IaC files therefore irrelevant |
| Dependency Scanning                             | Find security vulnerabilities in software dependencies  | Not included as don't have ultimate but would include to check dependencies |
| Container Scanning                              | Scans for vulnerabilities on parent docker images | Included as app uses parent docker image |
| Operational Container Scanning                  | Scan container images in a cluster | No cluster being used so irrelevant |
| Secret Detection                                | Scans repository for secrets. | Included to mitigate against this issue |
| API Fuzzing                                     | Causes unexpected behavior and errors in API backend by changing operational parameters | Requires test environment so not included but would include |

**General advisories for improving the security of the container** 

1. Show preference to trusted images (i.e. Docker Official Images) - Current version is offical, so matches criteria
2. Use up-to-date images (trade off between fixed-tag-version and latest (most likely to have sucurity patches)) - Current version is 1.18, released a year ago, latest is 1.21. Should pin to latest stable version
3. Enforce the principle of least privileged: process inside a docker container is ran as root, so should set a default user
4. Run containers as a non-root user 
5. Set resource limits (memory & CPU) - limits scope of malicious attacks
6. Use Docker security scanning tools - identify vulnerabilities within container images.